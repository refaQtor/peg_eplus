//rdmd -main -unittest -debug -cov -Jresources peg_eplus.d pegged/*.d

module peg_eplus;
import std.stdio, std.array, std.string, std.file, std.algorithm;
import pegged.grammar;

/++ CTFE embedding resources +/
alias sis = static immutable string;
sis idd_70 = import("Energy+70.idd");
sis idd_71 = import("Energy+71.idd");
sis idd_72 = import("Energy+72.idd");
sis idd_80 = import("Energy+80.idd");
sis idd_81 = import("Energy+81.idd");
sis idd_82 = import("Energy+82.idd");
sis idd_83 = import("Energy+83.idd");
static string[] idds = [idd_70, idd_71, idd_72, idd_80, idd_81, idd_82, idd_83]; 

mixin(grammar(`
IDD:

    GroupAttribute      <   Group
                        /   Comment
    ObjectAttribute     <   IDDObject
                        /   Memo
                        /   UniqueObject
                        /   RequiredObject
                        /   MinFields
                        /   Obsolete
                        /   Extensible
                        /   Format
                        /   ReferenceClassName
                        /   Comment
    FieldAttribute      <   Field
                        /   Note
                        /   RequiredField
                        /   BeginExtensible
                        /   Units
                        /   IpUnits
                        /   UnitsBasedOnField
                        /   Minimum
                        /   MinimumGreater
                        /   Maximum
                        /   MaximumLesser
                        /   Default
                        /   Deprecated
                        /   Autosizable
                        /   Autocalculatable
                        /   Type
                        /   Retaincase
                        /   Key
                        /   ObjectList
                        /   ExternalList
                        /   Reference
                        /   Comment
    Comment             <-  !.*\n         
    Group               <-  ":(\group)"
    IDDObject           <-  [A-Z][a-zA-Z]+
    Memo                <-  "\memo"
    UniqueObject        <-  "\unique-object"
    RequiredObject      <-  "\required-object"
    MinFields           <-  "\min-fields"
    Obsolete            <-  "\obsolete"
    Extensible          <-  "\extensible:"
    Format              <-  "\format"
    ReferenceClassName  <-  "\reference-class-name"      
    Field               <-  "\field"
    Note                <-  "\note"
    RequiredField       <-  "\required-field"
    BeginExtensible     <-  "\begin-extensible"
    Units               <-  "\units"
    IpUnits             <-  "\ip-units"
    UnitsBasedOnField   <-  "\unitsBasedOnField"
    Minimum             <-  "\minimum"
    MinimumGreater      <-  "\minimum>"
    Maximum             <-  "\maximum"
    MaximumLesser       <-  "\maximum<"
    Default             <-  "\default"
    Deprecated          <-  "\deprecated"
    Autosizable         <-  "\autosizable"
    Autocalculatable    <-  "\autocalculatable"
    Type                <-  "\type"
    Retaincase          <-  "\retaincase"
    Key                 <-  "\key"
    ObjectList          <-  "\object-list"
    ExternalList        <-  "\external-list"
    Reference           <-  "\reference"
`));


// void main(string[] args)
// {
//     pegIDDs();
// 
// 
// 
// }

void pegIDDs()
{
    foreach(idd; idds)
    {
        writeln("\n### BEGIN: " ~ idd ~ "\n");

        
                assert(true);
        writeln("\n### END: " ~ idd  ~ "\n");
    }
}

unittest
{
//   sis ls_test_files = "ls " ~ "*.idf";
    
    pegIDDs();

}
